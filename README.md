CONTENTS OF THIS FILE
---------------------
   
 * Introduction
  On this new web age data are not only stored inside database , but comes in various form
  like JSON,XML etc.
  Angular views module will convert JSON data to actual Sortable,Paginateable and Searchable
  UI.You can take any URL which displays JSON Data, and convert it to actual UI.
 
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers
