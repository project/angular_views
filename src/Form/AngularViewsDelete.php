<?php

namespace Drupal\angular_views\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implements InputDemo form controller.
 *
 * This example demonstrates the different input elements that are used to
 * collect data in a form.
 */
class AngularViewsDelete extends ConfirmFormBase {



    /**
     * The ID of the item to delete.
     *
     * @var string
     */
    protected $aid;

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'angular_views_admin_form_delete';
    }

    /**
     * {@inheritdoc}
     */
    public function getQuestion() {
        return t('Do you want to delete %aid?', array('%aid' => $this->aid));
    }

    /**
     * {@inheritdoc}
     */
    public function getCancelUrl() {
        return new Url('angular_views_admin');
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription() {
        return t('Only do this if you are sure that you want to delete id %aid!', array('%aid' => $this->aid));
    }

    /**
     * {@inheritdoc}
     */
    public function getConfirmText() {
        return t('Confirm');
    }

    /**
     * {@inheritdoc}
     */
    public function getCancelText() {
        return t('Cancel');
    }

    /**
     * {@inheritdoc}
     *
     * @param int $id
     *   (optional) The ID of the item to be deleted.
     */
    public function buildForm(array $form, FormStateInterface $form_state, $aid = NULL) {
       $this->aid = $aid;
        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

        $query = \Drupal::database()->delete('angular_views');
        $query->condition('aid', $this->aid);
        $query->execute();

        drupal_flush_all_caches();
        $form_state->setRedirect('angular_views_admin');

        
    }

}
