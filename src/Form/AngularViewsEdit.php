<?php

namespace Drupal\angular_views\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Implements InputDemo form controller.
 *
 * This example demonstrates the different input elements that are used to
 * collect data in a form.
 */
class AngularViewsEdit extends FormBase {

    protected $aid;

    /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'angular_views_admin_form_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $aid = NULL) {

     $this->aid = $aid;

     $query = \Drupal::database()->select('angular_views', 'av');
     $query->fields('av');
     $query->condition('av.aid', $this->aid);

     $av_data = $query->execute()->fetchAssoc();


    $current_protocol = explode('/',$_SERVER['SERVER_PROTOCOL']);
	
	$form['form_container'] = array(
	'#type' => 'fieldset',
	'#title' => $this->t('Set Angular views page'),
	'#weight' => -1,
	);
	
	// Select Protocol To communicate with.
    $form['form_container']['protocol'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Backend Service protocol'),
      '#options' => [
        'http' => $this->t('HTTP://'),
        'https' => $this->t('HTTPS://'),
      ],
      '#empty_option' => $this->t('- Select -'),
      '#description' => $this->t('Select Backend Service protocol on which angular will get request '),
	  '#required' => ($form_state->getValue('protocol')!= NULL ? $form_state->getValue('protocol'):$_SERVER['HTTP_HOST']),
	  '#default_value' => 'http'
    ];
	
	// Textfield.
    $form['form_container']['page_url'] = [
      '#type' => 'textfield',
      '#title' => t('Dispaly URL alias'),
      '#size' => 60,
      '#maxlength' => 128,
      '#description' => $this->t('Specify a path by which this UI can be accessed. For example, type "/angular-view" if you want to display it at URL: '.$GLOBALS['base_url'].'/angular-view'),
	  '#required' => TRUE,
      '#default_value' => $av_data['url']
    ];
	
	// Enter JSON backend URL.
    $form['form_container']['backend_url'] = [
      '#type' => 'textfield',
      '#title' => t('Backend JSON URL alias'),
      '#size' => 60,
      '#maxlength' => 128,
      '#description' => $this->t('Specify a path by which JSON data can be accessed. For example, type "/json-service" if your JSON data is showed at URL: '.$GLOBALS['base_url'].'/json-service'),
	  '#required' => TRUE,
      '#default_value' => $av_data['service_backend']
    ];


    // Add a submit button that handles the submission of the form.
    $form['form_container']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update'),
      '#description' => $this->t('Submit, #type = submit'),
    ];
	

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
  
	if($form_state->getValue('page_url')!= NULL && substr($form_state->getValue('page_url'),0,1)!="/"){
		$form_state->setErrorByName('page_url', t('Dispaly URL alias needs to start with a slash.'));
	}elseif($form_state->getValue('backend_url')!= NULL && substr($form_state->getValue('backend_url'),0,1)!="/"){
		$form_state->setErrorByName('backend_url', t('Backend JSON URL alias needs to start with a slash.'));
	}
  
 }
  
  

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Find out what was submitted.


      $query = \Drupal::database()->update('angular_views');
      $query->fields([
          'protocol' => $form_state->getValue('protocol'),
          'url' => $form_state->getValue('page_url'),
          'service_backend' => $form_state->getValue('backend_url'),
      ]);
      $query->condition('aid', $this->aid);
      $query->execute();


	drupal_flush_all_caches();
	drupal_set_message(t('Angular display updated successfully.'), 'status');
  }

}
