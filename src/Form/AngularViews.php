<?php

namespace Drupal\angular_views\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;


/**
 * Implements InputDemo form controller.
 *
 * This example demonstrates the different input elements that are used to
 * collect data in a form.
 */
class AngularViews extends FormBase {

	/**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'angular_views_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
	
	$current_protocol = explode('/',$_SERVER['SERVER_PROTOCOL']);
	
	$form['form_container'] = array(
	'#type' => 'fieldset',
	'#title' => $this->t('Set Angular views page'),
	'#weight' => -1,
	);
	
	// Select Protocol To communicate with.
    $form['form_container']['protocol'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Backend Service protocol'),
      '#options' => [
        'http' => $this->t('HTTP://'),
        'https' => $this->t('HTTPS://'),
      ],
      '#empty_option' => $this->t('- Select -'),
      '#description' => $this->t('Select Backend Service protocol on which angular will get request '),
	  '#required' => ($form_state->getValue('protocol')!= NULL ? $form_state->getValue('protocol'):$_SERVER['HTTP_HOST']),
	  '#default_value' => 'http'
    ];
	
	// Textfield.
    $form['form_container']['page_url'] = [
      '#type' => 'textfield',
      '#title' => t('Dispaly URL alias'),
      '#size' => 60,
      '#maxlength' => 128,
      '#description' => $this->t('Specify an alias by which this UI can be accessed. For example, type "/angular-view" if you want to display it at URL: '.$GLOBALS['base_url'].'/angular-view'),
	  '#required' => TRUE,
    ];
	
	// Enter JSON backend URL.
    $form['form_container']['backend_url'] = [
      '#type' => 'textfield',
      '#title' => t('Backend JSON URL alias'),
      '#size' => 60,
      '#maxlength' => 128,
      '#description' => $this->t('Specify an URL by which JSON data can be accessed. For example, type "'.$GLOBALS['base_url'].'/json-service" if your JSON data is showed at URL: '.$GLOBALS['base_url'].'/json-service. External URL is also allowed
       as long as they have valid JSON data'),
	  '#required' => TRUE,
    ];

    // Add a submit button that handles the submission of the form.
    $form['form_container']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#description' => $this->t('Submit, #type = submit'),
    ];
	
	 $header = [
      array('data' => $this->t('URL'), 'field' => 'url', 'sort' => 'asc'),
      array('data' => $this->t('Service Backend'),'field' => 'service_backend'),
	  array('data' => $this->t('Edit')),
	  array('data' => $this->t('Delete')),
    ];
	

	
    $query = \Drupal::database()->select('angular_views','av');

    $query->fields('av', array('aid'));
    $query->fields('av', array('url'));
	$query->fields('av', array('service_backend'));

	// The actual action of sorting the rows is here.
    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
                        ->orderByHeader($header);
	 // Limit the rows to 10 for each page.
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')
                        ->limit(10);
    $result = $pager->execute();
	
	// Populate the rows.
    $rows = array();
    foreach($result as $row) {
        $edit_l = \Drupal::l(t('Edit'), Url::fromUri('internal:/admin/config/user-interface/angular-views-management/edit/'.$row->aid));
        $del_l = \Drupal::l(t('Delete'), Url::fromUri('internal:/admin/config/user-interface/angular-views-management/delete/'.$row->aid));

      $rows[] = ['data' => [
        'url' => $GLOBALS['base_url'].$row->url,
        'service_backend' => $row->service_backend,
		'edit' => $edit_l,
		'del' => $del_l,
      ]];
    }

	
	// Generate the table.
    $build['config_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];
 
    // Finally add the pager.
    $build['pager'] = [
      '#type' => 'pager'
    ];

	
	$form['table'] = [
	'#markup' => \Drupal::service('renderer')->render($build),
	'#weight' => 0,
	];
    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {

      $alias = rtrim(trim($form_state->getValue('page_url')), " \\/");
      $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
      // Check if it exists within url_alias table
      $is_exists = \Drupal::service('path.alias_storage')->aliasExists($alias, $language);

      // Check if it exists within angular_views table
      $query = \Drupal::database()->select('angular_views', 'av');
      $query->addField('av', 'aid');
      $query->condition('av.url', $alias);
      $num_rows = $query->countQuery()->execute()->fetchField();


    if($form_state->getValue('page_url')!= NULL && substr($form_state->getValue('page_url'),0,1)!="/"){
		$form_state->setErrorByName('page_url', t('Dispaly URL alias needs to start with a slash.'));
	}elseif ($is_exists || $num_rows > 0){
        $form_state->setErrorByName('page_url', t('The alias is already in use.'));
    }elseif ($alias && $alias[0] !== '/') {
        $form_state->setError($element, t('The alias needs to start with a slash.'));
	}elseif($this->isJsonUrl($form_state->getValue('backend_url'))!== FALSE){
		$form_state->setErrorByName('backend_url', t($this->isJsonUrl($form_state->getValue('backend_url'))));
	}
  
 }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Find out what was submitted.

      $query = \Drupal::database()->insert('angular_views');
      $query->fields([
          'protocol',
          'url',
          'service_backend'
      ]);
      $query->values([
          $form_state->getValue('protocol'),
          $form_state->getValue('page_url'),
          $form_state->getValue('backend_url'),
      ]);
      $query->execute();

	drupal_flush_all_caches();
	drupal_set_message(t('Angular display created successfully.'), 'status');
  }


  public function isJsonUrl($url){

      $json_string = file_get_contents($url);

      json_decode($json_string);

      $err_code = FALSE;

      switch (json_last_error()) {
          case JSON_ERROR_NONE:
              $err_code = FALSE;
              break;
          case JSON_ERROR_DEPTH:
              $err_code = 'Maximum stack depth exceeded';
              break;
          case JSON_ERROR_STATE_MISMATCH:
              $err_code = 'Underflow or the modes mismatch';
              break;
          case JSON_ERROR_CTRL_CHAR:
              $err_code = 'Unexpected control character found';
              break;
          case JSON_ERROR_SYNTAX:
              $err_code = 'Syntax error, malformed JSON';
              break;
          case JSON_ERROR_UTF8:
              $err_code = 'Malformed UTF-8 characters, possibly incorrectly encoded';
              break;
          case JSON_ERROR_RECURSION:
              $err_code = 'One or more recursive references in the value to be encoded';
              break;
          case JSON_ERROR_INF_OR_NAN:
              $err_code = 'One or more NAN or INF values in the value to be encoded';
              break;
          case JSON_ERROR_UNSUPPORTED_TYPE:
              $err_code = 'A value of a type that cannot be encoded was given';
              break;
          case JSON_ERROR_INVALID_PROPERTY_NAME:
              $err_code = 'A property name that cannot be encoded was given';
              break;
          case JSON_ERROR_UTF16:
              $err_code = 'Malformed UTF-16 characters, possibly incorrectly encoded';
              break;
          default:
              $err_code = 'Unknown error';
              break;
      }

      return $err_code;

  }

}
